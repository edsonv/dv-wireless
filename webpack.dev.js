// webpack.dev.js - developmental builds

// Node modules
const merge = require('webpack-merge');
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

// Webpack plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Config files
const common = require('./webpack.common.js');
const pkg = require('./package.json');
const settings = require('./webpack.settings.js');

// Configure webpack-dev-server
const configureDevServer = () => {
    return {
        public: settings.devServerConfig.public(),
        contentBase: path.resolve(__dirname, settings.paths.src.base),
        host: settings.devServerConfig.host(),
        port: settings.devServerConfig.port(),
        // https: !!parseInt(settings.devServerConfig.https()),
        disableHostCheck: true,
        quiet: true,
        // hot: true,
        hotOnly: true,
        watchContentBase: true,
        stats: 'errors-only', // 'none' | 'errors-only' | 'minimal' | 'normal' | 'verbose'
        overlay: {
            // warnings: true,
            errors: true
        },
        watchOptions: {
            poll: !!parseInt(settings.devServerConfig.poll()),
            ignored: '/node_modules/'
        },
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    };
};

// Configure Pug loader
const configurePugLoader = () => {
    return {
        test: /\.pug$/,
        use: 'pug-loader'
    };
};

// Configure Postcss loader
const configurePostcssLoader = () => {
    return {
        test: /\.(css|scss)$/,
        use: [
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                    sourceMap: true,
                    importLoaders: 2
                }
            },
            // Is this useful?
            // {
            //     loader: 'resolve-url-loader'
            // },
            {
                loader: 'postcss-loader',
                options: {
                    sourceMap: true
                }
            },
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ]
    };
};

// Configure font loader
const configureFontLoader = () => {
    return {
        test: /\.(otf|eot|ttf|woff|woff2)$/i,
        // exclude: 'src/img/',
        use: {
            loader: 'file-loader',
            options: {
                name: 'fonts/[name].[hash:11].[ext]'
                // outputPath: 'fonts/'
            }
        }
    };
};

// Configure php loader
const configurePHPLoader = () => {
    return {
        test: /\.(php)$/i,
        use: {
            loader: 'file-loader',
            options: {
                name: '[name].[hash:11].[ext]'
            }
        }
    };
};

// Configure image loader
const configureImageLoader = () => {
    return {
        test: /\.(jp?g|svg|png|gif|webp)$/i,
        // exclude: 'src/fonts/',
        use: {
            loader: 'file-loader',
            options: {
                name: 'img/[name].[hash:11].[ext]'
            }
        }
    };
};

// Configure HTML webpack plugin
const configureHtmlWebpackPlugin = () => {
    const templates = fs.readdirSync(
        path.resolve(__dirname, settings.paths.templates.views)
    );

    return templates.map(item => {
        // Split names and extension
        const parts = item.split('.');
        const name = parts[0];
        const extension = parts[1];

        return new HtmlWebpackPlugin({
            filename: `${name}.html`,
            template: path.resolve(
                __dirname,
                `${settings.paths.templates.views}${name}.${extension}`
            ),
            chunks: [`${name}`]
        });
    });
};

// Development module exports
module.exports = merge(common, {
    output: {
        filename: 'js/[name].[hash:11].js',
        publicPath: settings.devServerConfig.public() + '/'
    },
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: configureDevServer(),
    bail: true,
    cache: true,
    parallelism: 5,
    performance: {
        hints: 'warning' // 'error' for production
    },
    module: {
        rules: [
            configurePostcssLoader(),
            configureFontLoader(),
            configureImageLoader(),
            configurePugLoader(),
            configurePHPLoader()
        ]
    },
    plugins: [new webpack.HotModuleReplacementPlugin()].concat(
        configureHtmlWebpackPlugin()
    )
});
