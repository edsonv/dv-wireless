// Toggle form function
$(function toggleForm() {
    'use-strict';

    $('[data-toggle="form"]').on('click', function(event) {
        $('#form').css('display', 'block');
        event.stopPropagation();
    });

    $(document).on('click', function() {
        $('#form').css('display', 'none');
    });

    $('.close').on('click', function() {
        $('#form').css('display', 'none');
    });

    $('#form-inner').on('click', function(event) {
        event.stopPropagation();
    });
});

//  Smooth scrolling function
$('.nav-link').on('click', function(event) {
    event.preventDefault();

    let selector = $($(this).attr('href')).offset().top;
    let navbarMargin = $('#main_menu').outerHeight(true);

    $('html, body').animate(
        {
            scrollTop: selector - navbarMargin
        },
        1000,
        'linear'
    );
    toggler();
});

// Toggle menu function
function toggler() {
    let navbarMargin = $('#main_menu').outerHeight(true);
    $('.offcanvas-collapse').toggleClass('open');
    $('.navbar-toggler').toggleClass('rotate');
    $('.offcanvas-collapse').css('top', navbarMargin);
}

$('[data-toggle="offcanvas"]').on('click', function() {
    toggler();
});

// Lazy loading
document.addEventListener('DOMContentLoaded', function() {
    let lazyImages = [].slice.call(document.querySelectorAll('.lazy'));
    let active = false;

    const lazyLoad = function() {
        if (active === false) {
            active = true;

            setTimeout(function() {
                lazyImages.forEach(function(lazyImage) {
                    if (
                        lazyImage.getBoundingClientRect().top <=
                            window.innerHeight &&
                        lazyImage.getBoundingClientRect().bottom >= 0 &&
                        getComputedStyle(lazyImage).display !== 'none'
                    ) {
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.srcset = lazyImage.dataset.srcset;
                        lazyImage.classList.remove('lazy');

                        lazyImages = lazyImages.filter(function(image) {
                            return image !== lazyImage;
                        });

                        if (lazyImages.length === 0) {
                            document.removeEventListener('scroll', lazyLoad);
                            window.removeEventListener('resize', lazyLoad);
                            window.removeEventListener(
                                'orientationchange',
                                lazyLoad
                            );
                        }
                    }
                });

                active = false;
            }, 200);
        }
    };

    document.addEventListener('scroll', lazyLoad);
    window.addEventListener('resize', lazyLoad);
    window.addEventListener('orientationchange', lazyLoad);
});

$('img.card-img-top').one('load', function() {
    $(this).addClass('animated rotateInDownLeft');
});

// Metricool, herramienta de seguimiento
function loadScript(a) {
    var b = document.getElementsByTagName('head')[0],
        c = document.createElement('script');
    (c.type = 'text/javascript'),
        (c.src = 'https://tracker.metricool.com/resources/be.js'),
        (c.onreadystatechange = a),
        (c.onload = a),
        b.appendChild(c);
}
$(
    loadScript(function() {
        beTracker.t({ hash: 'a20e444704f2036ef06aacce051fe23c' });
    })
);
$(function() {
    $('[data-toggle="popover"]').popover({
        html: true,
        content: function() {
            return $('#popover-content').html();
        }
    });
});
