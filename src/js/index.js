// Import bootstrap
import 'bootstrap';
// import 'bootstrap/js/dist/button'
// import 'bootstrap/js/dist/carousel'
// import 'bootstrap/js/dist/collapse'
// import 'bootstrap/js/dist/util'

// Import custom styles
import '../scss/index.scss';

// Import offcanvas menu control
import './form';
import './util';

// import '../img/favicon.png'

// Enabling HMR
if (module.hot) {
    module.hot.accept(console.log('Aceptando nuevos cambios...'));
}
