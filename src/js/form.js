$(function () {
    'use strict'
    //- Obtenemos el formulario.
    var form = $('.form')

    //- Obtenemos el div de mensajes.
    var message = $('.messages')

    var form_data

    console.log("se ha esjecutado el script form.js")

    $(form).submit(function (event) {
        event.preventDefault()
        form_data = $(form).serialize()
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: form_data,
            statusCode: {
                200: function (response) {
                    $(message).fadeIn().removeClass('alert-danger d-none').addClass('alert-success')
                    $(message).text(response)
                },
                202: function (data) {
                    $(message).fadeIn().removeClass('alert-success d-none').addClass('alert-danger')
                    $(message).text(data.responseText)
                }
            }
        })
    })
})