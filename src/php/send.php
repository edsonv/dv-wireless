<?php
    $to = "info@dvwireless.com";
    $subject = "You have received a comment from DV Wireless";

    $name = $_POST["name"];
    $email = $_POST["email"];
    $phone = $_POST["phone"];
    $comment = $_POST["comment"];
    $website = $_POST["website"];
    
    // Headers
    $headers = "From: {$name} <{$email}> \r\n";
    $headers .= "Bcc: evargas@swsnet.com.ve \r\n";
    $headers .= "Reply-To: {$email} \r\n";
    $headers .= "X-Mailer: PHP/" . phpversion();

    // Message
    $message = "Name: {$name} \r\n";
    $message .= "Email: {$email} \r\n";
    $message .= "Phone: {$phone} \r\n";
    $message .= "Comment: {$comment} \r\n";
    
    if(empty($name)) {
        echo "Please enter your name.";
    } elseif (empty($email)) {
        echo "Please enter your email.";
    } elseif (empty($phone)) {
        echo "Please enter your phone number.";
    } elseif (empty($comment)) {
        echo "Please enter your message.";
    } elseif (!empty($website)) {
        echo "Are you a robot?";
        die();
    } else {
        $success = mail ($to, $subject, $message, $headers);
        if ($success) {
            http_response_code(200);
            echo "Thank you! Your message have been sent.";
        } else {
            http_response_code(202);
            echo "Oops! An error has occurred, your message could not be sent.";
        }
    }
?>