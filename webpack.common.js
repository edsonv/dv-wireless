// webpack.common.js - common webpack config


// Node modules
const path = require('path')
const webpack = require('webpack')

// Webpack plugins
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

// config files
const pkg = require('./package.json');
const settings = require('./webpack.settings.js');

// Configure Babel loader
const configureBabelLoader = (browserList) => {
    return {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
            loader: 'babel-loader',
            options: {
                presets: [
                    [
                        '@babel/preset-env',
                        {
                            modules: false,
                            // corejs: 2,
                            useBuiltIns: 'usage',
                            targets: {
                                browsers: browserList
                            }
                        }
                    ]
                ],
                plugins: [
                    '@babel/plugin-syntax-dynamic-import',
                    [
                        '@babel/plugin-transform-runtime',
                        {
                            'regenerator': true
                        }
                    ]
                ]
            }
        }
    }
}



// Configure Entries
const configureEntries = () => {
    let entries = {};
    for (const [key, value] of Object.entries(settings.entries)) {
        entries[key] = path.resolve(__dirname, settings.paths.src.js + value);
    }

    return entries;
};

// Configure Manifest
const configureManifest = (fileName) => {
    return {
        fileName: fileName,
        // basePath: settings.manifestConfig.basePath,
        basePath: '',
        map: (file) => {
            file.name = file.name.replace(/(\.[a-f0-9]{32})(\..*)$/, '$2');
            return file;
        },
    };
};

// The base webpack config
module.exports = {
    name: pkg.name,
    entry: configureEntries(),
    output: {
        path: path.resolve(__dirname, settings.paths.dist.base),
        publicPath: settings.urls.publicPath()
    },
    module: {
        rules: [
            configureBabelLoader(Object.values(pkg.browserslist)),
        ],
    },
    plugins: [
        new CopyWebpackPlugin(
            settings.copyWebpackConfig
        ),
        new ManifestPlugin(
            configureManifest('manifest.json')
        ),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            // 'window.jQuery': 'jquery'
        })
    ]
}