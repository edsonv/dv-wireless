// webpack.prod.js - production builds


// node modules
const fs = require('fs')
const glob = require('glob-all')
const merge = require('webpack-merge')
const path = require('path')
const webpack = require('webpack')

// webpack plugins
const CriticalCssPlugin = require('critical-css-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CreateSymlinkPlugin = require('create-symlink-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const WhitelisterPlugin = require('purgecss-whitelister')
const WebappWebpackPlugin = require('webapp-webpack-plugin')
// const WorkboxPlugin = require('workbox-webpack-plugin')


// config files
const common = require('./webpack.common.js')
const pkg = require('./package.json')
const settings = require('./webpack.settings.js')

// Configure Critical CSS
const configureCriticalCss = () => {
    return (settings.criticalCssConfig.pages.map((row) => {
        const criticalSrc = settings.urls.critical + row.url
        const criticalDest = settings.criticalCssConfig.base + row.template + settings.criticalCssConfig.suffix
        let criticalWidth = settings.criticalCssConfig.criticalWidth
        let criticalHeight = settings.criticalCssConfig.criticalHeight
        // Handle Google AMP templates
        if (row.template.indexOf(settings.criticalCssConfig.ampPrefix) !== -1) {
            criticalWidth = settings.criticalCssConfig.ampCriticalWidth
            criticalHeight = settings.criticalCssConfig.ampCriticalHeight
        }
        console.log("source: " + criticalSrc + " dest: " + criticalDest)
        return new CriticalCssPlugin({
            base: './',
            src: criticalSrc,
            dest: criticalDest,
            extract: true,
            inline: true,
            minify: true,
            width: criticalWidth,
            height: criticalHeight,
        })
    })
    )
}

// Configure Clean webpack
const configureCleanWebpack = () => {
    return {
        root: path.resolve(__dirname, settings.paths.dist.base),
        verbose: true,
        dry: false
    }
}

// Configure HTML webpack plugin
const configureHtmlWebpackPlugin = () => {
    const templates = fs.readdirSync(path.resolve(__dirname, settings.paths.templates.views))

    return templates.map(item => {
        // Split names and extension
        const parts = item.split('.')
        const name = parts[0]
        const extension = parts[1]

        return new HtmlWebpackPlugin({
            template: path.resolve(__dirname, `${settings.paths.templates.views}${name}.${extension}`),
            filename: `${name}.html`,
            chunks: [`${name}`],
            inject: true
        })
    })
}

// Configure Pug loader
const configurePugLoader = () => {
    return {
        test: /\.pug$/,
        use: 'pug-loader'
    }
}

// Configure image loader
const configureImageLoader = () => {
    return {
        test: /\.(jp?g|png|gif|svg|webp)$/,
        exclude: path.resolve(__dirname, 'fonts/'),
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[contenthash].[ext]'
                }
            },
            {
                loader: 'img-loader',
                options: {
                    plugins: [
                        require('imagemin-mozjpeg')({
                            progressive: true,
                            arithmetic: false
                        }),
                        require('imagemin-optipng')({
                            optimizationLevel: 4
                        })
                    ]
                }
            }
        ]
    }
}

// Configure font loader
const configureFontLoader = () => {
    return {
        test: /\.(otf|eot|svg|ttf|woff|woff2)$/i,
        exclude: path.resolve(__dirname, 'src/img/'),
        use: {
            loader: 'file-loader',
            options: {
                name: '[name].[contenthash].[ext]',
                outputPath: 'fonts/'
            }
        }
    }
}

// Configure php loader
const configurePHPLoader = () => {
    return {
        test: /\.(php)$/i,
        use: {
            loader: 'file-loader',
            options: {
                name: '[name].[hash:11].[ext]',
            }
        }
    }
}

// Configure optimization
const configureOptimization = () => {
    return {
        splitChunks: {
            cacheGroups: {
                default: false,
                common: false,
                // styles: {
                //     name: '[name]-styles.[contenthash]',
                //     test: /\.(scss|css)$/,
                //     chunks: 'all',
                //     enforce: true
                // }
            }
        },
        minimizer: [
            new TerserPlugin(
                configureTerser()
            ),
            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: {
                    map: {
                        inline: false,
                        annotation: true
                    },
                    safe: true,
                    discardComments: true
                }
            })
        ]
    }
}

// Configure postcss
const configurePostcssLoader = () => {
    return {
        test: /\.?css$/,
        use: [
            {
                loader: MiniCssExtractPlugin.loader,
                options: {
                    publicPath: '../'
                }
            },
            {
                loader: 'css-loader',
                options: {
                    sourceMap: true,
                    importLoaders: 2,
                }
            },
            {
                loader: 'postcss-loader',
                options: {
                    sourceMap: true,
                }
            },
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ]
    }
}

// Configure PurgeCSS
const configurePurgeCss = () => {
    let paths = [];
    // Configure whitelist paths
    for (const [key, value] of Object.entries(settings.purgeCssConfig.paths)) {
        paths.push(path.join(__dirname, value));
    }

    return {
        paths: glob.sync(paths),
        whitelist: WhitelisterPlugin(settings.purgeCssConfig.whitelist),
        whitelistPatterns: settings.purgeCssConfig.whitelistPatterns,
        // extractors: [
        //     {
        //         extensions: settings.purgeCssConfig.extensions
        //     }
        // ]
    };
};

// Configure Workbox service worker
// const configureWorkbox = () => {
//     let config = settings.workboxConfig

//     return config
// }

// Configure terser
const configureTerser = () => {
    return {
        cache: true,
        parallel: true,
        sourceMap: true
    }
}

const configureWebapp = () => {
    return {
        logo: settings.webappConfig.logo,
        prefix: settings.webappConfig.prefix,
        cache: false,
        inject: 'force',
        favicons: {
            appName: pkg.name,
            appDescription: pkg.description,
            developerName: pkg.author.name,
            developerURL: pkg.author.url,
            path: settings.paths.dist.base
        }
    }
}

// Production module exports
module.exports = merge(common, {
    output: {
        filename: path.join('js', '[name].[contenthash].js')
    },
    mode: 'production',
    devtool: 'source-map',
    optimization: configureOptimization(),
    module: {
        rules: [
            configurePostcssLoader(),
            configurePHPLoader(),
            configureFontLoader(),
            configureImageLoader(),
            configurePugLoader(),
        ],
    },
    plugins: [
        new CleanWebpackPlugin(
            settings.paths.dist.clean, configureCleanWebpack()
        ),
        new MiniCssExtractPlugin({
            path: path.resolve(__dirname, settings.paths.dist.base),
            filename: path.join('css', '[name].[contenthash].css')
        }),
        new PurgecssPlugin(
            configurePurgeCss()
        ),
        new WebappWebpackPlugin(
            configureWebapp()
        ),
        new CreateSymlinkPlugin(
            settings.createSymlinkConfig,
            true
        ),
        // new WorkboxPlugin.GenerateSW(
        //     configureWorkbox()
        // )
    ]
        .concat(
            configureHtmlWebpackPlugin(),
            // configureCriticalCss(), //Activate when building to publish

        )
})