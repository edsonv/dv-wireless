// config file
const settings = require('./webpack.settings.js')

// common config
module.exports = {
    plugins: [
        // require('autoprefixer'), // Autoprefixer is built-in postcss and does no need to be installed as a plugin
        require('postcss-preset-env')({
            stage: 0,
        }),
        require('postcss-font-magician')({
            custom: {
                'Myriad Pro': {
                    variants: {
                        normal: {
                            300: {
                                url: {
                                    otf: './src/fonts/MyriadPro-Light.otf'
                                }
                            },
                            400: {
                                url: {
                                    otf: './src/fonts/MyriadPro-Regular.otf'
                                }
                            },
                            600: {
                                url: {
                                    otf: './src/fonts/MyriadPro-Semibold'
                                }
                            },
                            700: {
                                url: {
                                    otf: './src/fonts/MyriadPro-Bold.otf'
                                }
                            }
                        }
                    }
                }
            },
            variants: {
                'Myriad Pro': {
                    '300': [],
                    '400': [],
                    '600': []
                },
                'Hind': {
                    '300': [],
                    '400': [],
                    '600': []
                },
                'Mukta': {
                    '300': [],
                    '400': [],
                    '600': []
                }
            },
            display: 'block',
        }),
        // require('@fullhuman/postcss-purgecss')({
        //     content: settings.purgeCssConfig.paths,
        //     whitelist: settings.purgeCssConfig.whitelist,
        //     whitelistPatterns: settings.purgeCssConfig.patterns
        // })
    ]
}