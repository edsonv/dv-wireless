// webpack.settings.js - webpack settings config

// node modules
require('dotenv').config();
const pkg = require('./package.json');

// Webpack settings exports
// noinspection WebpackConfigHighlighting
module.exports = {
    name: 'DV Wireless',
    // copyright: 'Example Company, Inc.',
    paths: {
        src: {
            base: './src/',
            scss: './src/scss/',
            js: './src/js/'
        },
        dist: {
            base: './dist',
            clean: ['./img', './criticalcss', './css', './js']
        },
        templates: {
            includes: './src/templates/includes/',
            views: './src/templates/views/'
        }
    },
    urls: {
        live: 'https://example.com/',
        local: 'http://example.test/',
        critical: 'http://dvwireless.com/',
        publicPath: () => process.env.PUBLIC_PATH || ''
    },
    // vars: {
    //     cssName: 'styles'
    // },
    entries: {
        index: 'index.js'
    },
    copyWebpackConfig: [
        {
            from: './src/js/workbox-catch-handler.js',
            to: 'js/[name].[ext]'
        }
    ],
    criticalCssConfig: {
        base: './dist/criticalcss/',
        suffix: '_critical.min.css',
        criticalHeight: 1200,
        criticalWidth: 1200,
        ampPrefix: 'amp_',
        ampCriticalHeight: 19200,
        ampCriticalWidth: 600,
        pages: [{ url: '', template: 'index' }]
    },
    devServerConfig: {
        public: () => process.env.DEVSERVER_PUBLIC || 'http://localhost:8080',
        host: () => process.env.DEVSERVER_HOST || 'localhost',
        poll: () => process.env.DEVSERVER_POLL || false,
        port: () => process.env.DEVSERVER_PORT || 8080,
        https: () => process.env.DEVSERVER_HTTPS || false,
        open: () => process.env.DEVSERVER_OPEN || 'firefox-developer'
    },
    manifestConfig: {
        basePath: ''
    },
    purgeCssConfig: {
        paths: ['./src/templates/**/*.{pug,html}'],
        whitelist: [
            './src/scss/**/*.{css,scss}',
            './node_modules/bootstrap/scss/_dropdown.scss',
            './node_modules/bootstrap/scss/_carousel.scss',
            './node_modules/bootstrap/scss/_popover.scss'
        ],
        whitelistPatterns: [
            /(btn).*/,
            /(success).*/,
            /(error).*/,
            /(animated).*/,
            /(rotateInDownLeft).*/
        ],
        extensions: ['html', 'pug', 'js']
    },
    saveRemoteFileConfig: [
        {
            url: 'https://www.google-analytics.com/analytics.js',
            filepath: 'js/analytics.js'
        }
    ],
    createSymlinkConfig: [
        {
            origin: 'img/favicons/favicon.ico',
            symlink: './favicon.ico'
        }
    ],
    webappConfig: {
        logo: './src/img/favicon.png',
        prefix: 'img/favicons/'
    },
    workboxConfig: {
        swDest: 'sw.js',
        precacheManifestFilename: 'js/precache-manifest.[manifestHash].js',
        importScripts: ['/dist/workbox-catch-handler.js'],
        exclude: [
            /\.(png|jpe?g|gif|svg|webp)$/i,
            /\.map$/,
            /^manifest.*\\.js(?:on)?$/
        ],
        globDirectory: './',
        globPatterns: ['offline.html', 'offline.svg'],
        offlineGoogleAnalytics: true,
        runtimeCaching: [
            {
                urlPattern: /\.(?:png|jpg|jpeg|svg|webp)$/,
                handler: 'cacheFirst',
                options: {
                    cacheName: 'images',
                    expiration: {
                        maxEntries: 20
                    }
                }
            }
        ]
    }
};
